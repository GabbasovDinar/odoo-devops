Odoo DevOps
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   first-steps
   Docker/index
   Kubernetes/index
   Gitlab-ci/index
   ifttt/index
